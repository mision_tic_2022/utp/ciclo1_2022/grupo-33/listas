
miLista = ['Juan', 'Ana', 'Alfonso', 'Angélica', 'Andrés', 'María']

#Recorrer lista
for x in miLista:
    print(x)

numeros = [10,200,400,500]
for i in numeros:
    print(i)

print('-----------------')

booleanos = [True, False, False, True, True, False]
for b in booleanos:
    if b == True:
        print('Es verdad')
    else:
        print('Es falso')

print('--------------')

#Recorrer un rango
for n in range(0, 1000):
    print(n)
