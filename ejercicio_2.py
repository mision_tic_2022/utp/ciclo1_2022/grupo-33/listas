'''
1) Crear una lista con todos los números pares del 1 al 1000
2) Crear una lista con todos los números impares del 1 al 1000
'''
#Solución de Royer
numero_pares = list()

for x in range(1,101):
    if x%2 == 0:
        numero_pares.append(x)
print(numero_pares)

#Solución de Karol Cardenas
lista_par = []
lista_impar = []

for num in range (1,1000):
    if num % 2 == 0:
        lista_par.append (num)
    else:
        lista_impar.append (num)

print ('NÚMEROS PARES: ',lista_par)
print ('NÚMEROS IMPARES: ',lista_impar)